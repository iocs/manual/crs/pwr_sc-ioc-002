#
# Module: essioc
#
require essioc

#
# Module: modbus
#
require modbus


#
# ESSIOC
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# RTU-to-TCP Modbus gateway
#
iocshLoad("${E3_CMD_TOP}/iocsh/gateway.iocsh", "IPADDR = cryo-med-pwrmeter-gw.tn.esss.lu.se")

#
# Device: Pwr-L1U2:CnPw-EA-ACM210
#
iocshLoad("${E3_CMD_TOP}/iocsh/imepowermeter.iocsh", "DEVICENAME = Pwr-L1U2:CnPw-EA-ACM210, SLAVE_ADDRESS = 10, POLL_4096 = 3100, POLL_4130 = 3200")

#
# Device: Pwr-L1U3:CnPw-EA-ACM230
#
iocshLoad("${E3_CMD_TOP}/iocsh/imepowermeter.iocsh", "DEVICENAME = Pwr-L1U3:CnPw-EA-ACM230, SLAVE_ADDRESS = 11, POLL_4096 = 3000, POLL_4130 = 3100")

#
# Device: Pwr-L1U6:CnPw-EA-ACM240
#
iocshLoad("${E3_CMD_TOP}/iocsh/imepowermeter.iocsh", "DEVICENAME = Pwr-L1U6:CnPw-EA-ACM240, SLAVE_ADDRESS = 12, POLL_4096 = 3100, POLL_4130 = 3000")

#
# Device: Pwr-L1U7:CnPw-EA-ACM290
#
iocshLoad("${E3_CMD_TOP}/iocsh/imepowermeter.iocsh", "DEVICENAME = Pwr-L1U7:CnPw-EA-ACM290, SLAVE_ADDRESS = 13, POLL_4096 = 3000, POLL_4130 = 3200")

#
# Device: Pwr-L10U2:CnPw-EA-TMM211
#
iocshLoad("${E3_CMD_TOP}/iocsh/imepowermeter.iocsh", "DEVICENAME = Pwr-L10U2:CnPw-EA-TMM211, SLAVE_ADDRESS = 14, POLL_4096 = 3200, POLL_4130 = 3100")

#
# Device: Pwr-L10U5:CnPw-EA-TMM212
#
iocshLoad("${E3_CMD_TOP}/iocsh/imepowermeter.iocsh", "DEVICENAME = Pwr-L10U5:CnPw-EA-TMM212, SLAVE_ADDRESS = 15, POLL_4096 = 3000, POLL_4130 = 3200")

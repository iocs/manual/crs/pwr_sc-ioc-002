# IOC to control the ACCP and TMCP medium voltage motors Power Meters

This IOC uses a Conto D4-Pt power meter from IME via an RTU-to-TCP modbus gateway.

- User manual: [IME-Conto-D4-PowerMeter.pdf](doc/IME-Conto-D4-PowerMeter.pdf)


## Responsible

Miklós Boros ([email](mailto://miklos.boros@ess.eu))

Dependencies
============

*  `modbus`


## Controlled devices

*   Pwr-L1U2:CnPw-EA-ACM210
*   Pwr-L1U3:CnPw-EA-ACM230
*   Pwr-L1U6:CnPw-EA-ACM240
*   Pwr-L10U2:CnPw-EA-TMM211
*   Pwr-L10U5:CnPw-EA-TMM212


## Confgiuration


Load the configuration for the modbus gateway device:

```
    iocshLoad("$(E3_CMD_TOP)/gateway.iocsh", "IPADDR=cryo-med-pwrmeter-gw")
```

Parameters:
- IPADDR: The IP address of the gateway


Then load the database for the slave devices (`Pwr-L1U2:CnPw-EA-ACM210`):

```
    iocshLoad("$(E3_CMD_TOP)/imepowermeter.iocsh", "DEVICENAME=Pwr-L1U2:CnPw-EA-ACM210, SLAVE_ADDRESS=10, POLL_4096=3100, POLL_4130=3200")
```

Parameters:
- DEVICENAME: The name of the device as registered in the Naming service
- SLAVE_ADDRESS: The modbus slave address of the device
- POLL_4096:  The poll interval for registers starting at 4096
- POLL_4130:  The poll interval for registers starting at 4130
